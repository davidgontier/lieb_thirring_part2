####################################
# This code is a finite element one to compute the best LT constant in 2D
# Written by D. Gontier on January 18 2019.

import numpy as np
import numpy.fft as fft
import scipy.sparse.linalg as LA
import matplotlib.pyplot as plt
import scipy.interpolate as interpolate
import scipy.special as special


# Some constant for the initial lattice
# the lattice
a = np.sqrt(2/np.sqrt(3))
a1 = a*np.array([1,0])
a2 = a*np.array([1/2, np.sqrt(3)/2])
# chosen so that it is of area 1.

# The dual lattice
v1 = 2*np.pi*np.array([1, -1/np.sqrt(3)])/a
v2 = 2*np.pi*np.array([0, 2/np.sqrt(3)])/a
# the area if (2 \pi)^2


###############################
def get_kinetic_matrix(k, Nb = 50):
    # k is the kpts in the Brillouin zone
    # Nb is the size of the basis per direction, so Nb**2 elements.

    KK = np.linspace(0, Nb-1, Nb)
    kx, ky = np.meshgrid(KK, KK)

    n1 = (kx*v1[0] + ky*v2[0] - k[0])**2 + (kx*v1[1] + ky*v2[1] - k[1])**2
    n2 = ((kx-Nb)*v1[0] + ky*v2[0] - k[0])**2 + ((kx-Nb)*v1[1] + ky*v2[1] - k[1])**2
    n3 = (kx*v1[0] + (ky-Nb)*v2[0] - k[0])**2 + (kx*v1[1] + (ky - Nb)*v2[1]- k[1])**2
    n4 = ((kx-Nb)*v1[0] + (ky - Nb)*v2[0] - k[0])**2 + ((kx-Nb)*v1[1] + (ky - Nb)*v2[1] - k[1])**2

    return np.minimum( np.minimum(n1, n2), np.minimum(n3, n4))

###############################
# The operator -\Delta
# with Bloch transform, it becomes (K + k)^2

def get_mDelta(k, Nb = 50):
    _kinetic = get_kinetic_matrix(k, Nb)
    def mult_mDelta(ul):
        u = np.reshape(ul, (Nb,Nb))
        U = fft.ifft2(u)
        mDeltaU = _kinetic*U
        mdeltau = fft.fft2(mDeltaU)
        return np.reshape(mdeltau, Nb**2)
    return LA.LinearOperator((Nb**2, Nb**2), matvec = mult_mDelta)


##################################
# The multiplication by V operator
def get_Vop(V):
    Nb = np.shape(V)[0]
    def mult_V(ul):
        u = np.reshape(ul, (Nb, Nb))
        Vu = V*u
        return np.reshape(Vu, Nb**2)
    return LA.LinearOperator((Nb**2, Nb**2), matvec = mult_V)


#####################################
# From rho, the best V is of the form V = \lambda \rho^{1/\kappa}
# Here, we fix I = \int V^{\kappa + 1}
def get_V_LT(rho, kappa, Itarget):
    # I is \int V^{\kappa + 1}
    # We need to optimise in I at the end!
    V = rho**(1/kappa)
    I = sum(V**(kappa+1))/Nb**2
    return V*(Itarget/I)**(1/(kappa+1))

#######################################
# The Hamiltonian operator
def get_H_LT(k, V):
    Nb = np.shape(V)[0]
    mDelta = get_mDelta(k, Nb)
    Vop = get_Vop(V)
    return mDelta - Vop #sign - since Vop is positive

#######################################
# The LT semi-classical constant
def get_Lsc(kappa):
    return 1/(4*np.pi)*special.gamma(kappa+1)/special.gamma(kappa+2) #semi classical cst


########################################
def update_rho(rho, kappa, epsF, Itarget, Nkpts):
    # We work in grand canonic for simplicity, so epsF is needed
    # Nkpts is the number of Kpts per direction in the BZ.
    # a good choice should be \eps = 0


    Nbands = 7 # the 7 first bands, due to symmetry

    kk1, kk2 = np.linspace(0, 1, Nkpts), np.linspace(0, 1, Nkpts)
    V = get_V_LT(rho, kappa, Itarget)
    Nb = np.shape(V)[0]

    energy = 0 # the sum of \sum \lambda_n^\kappa
    Nel = 0
    rho = np.zeros((Nb,Nb))

    for k1 in kk1:
        for k2 in kk2:
            k = k1*v1 + k2*v2
            H = get_H_LT(k, V)
            W, U = LA.eigsh(H, Nbands, which='SA')

            for i in range(Nbands):
                if W[i] < epsF:
                    uil = U[:,i]
                    ui = np.reshape(uil, (Nb,Nb))

                    energy += abs(W[i])**kappa
                    Nel += 1
                    rho += abs(W[i])**(kappa-1)* abs(ui)**2
    # end loop
    # Normalisation
    energy /= Nkpts**2
    Nel /= Nkpts**2
    rho /= Nkpts**2

    return energy, Nel, rho







k = 0*v1 + 0*v2
H = get_mDelta(k)
w, v = LA.eigsh(H, 10, which='SA')
print(w)