# The nonlinear Schrödinger equation for orthonormal functions: II. Application to Lieb-Thirring 

*The nonlinear Schrödinger equation for orthonormal functions: II. Application to Lieb-Thirring inequalities.*
Rupert L. Frank, David Gontier, Mathieu Lewin
Commun. Math. Phys. 384, 1783–1828 (2021). 

journal version: https://link.springer.com/article/10.1007/s00220-021-04039-5

arxiv version: https://arxiv.org/abs/2002.04964

Author: David Gontier (2021)